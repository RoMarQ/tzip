import smartpy as sp
FA2 = sp.io.import_template("FA2.py")

TTxs = sp.TRecord(
    amount=sp.TNat,
    callback=sp.TAddress,
    data=sp.TBytes,
    to_=sp.TAddress,
    token_id=sp.TNat,
).right_comb()

TTransfer_and_call = sp.TList(
    sp.TRecord(
        from_=sp.TAddress,
        txs=sp.TList(TTxs)
    )
)

TCallback = sp.TRecord(
    amount=sp.TNat,
    data=sp.TBytes,
    receiver=sp.TAddress,
    sender=sp.TAddress,
    token_id=sp.TNat,
).right_comb()


class FA2_transfer_and_call(FA2.FA2):

    @sp.sub_entry_point
    def _transfer(self, params):
        super().transfer(params)

    @sp.entry_point
    def transfer(self, params):
        self._transfer(params)

    @sp.entry_point
    def transfer_and_call(self, batchs):
        """
            Perform a FA2 transfer and
            Call the `callback` address with the `data` bytes args.
        """
        sp.set_type(batchs, TTransfer_and_call)

        # Transfer
        with sp.for_('batchs', batchs) as batch:
            with sp.for_('transaction', batch.txs) as tx:
                self._transfer([
                    sp.record(
                        from_=batch.from_,
                        txs=[
                            sp.record(
                                to_=tx.to_,
                                token_id=tx.token_id,
                                amount=tx.amount
                            )
                        ]
                    )
                ])

                # Callback
                callback = sp.contract(TCallback, tx.callback).open_some(
                    "FA2_WRONG_CALLBACK_INTERFACE")
                args = sp.record(
                    amount=tx.amount,
                    data=tx.data,
                    receiver=tx.to_,
                    sender=batch.from_,
                    token_id=tx.token_id,
                )
                sp.transfer(args, sp.tez(0), callback)


class Receiver(sp.Contract):
    def __init__(self, fa2, from_, token_id, amount):
        self.init(fa2=fa2,
                  token_id=token_id,
                  from_=from_,
                  amount=amount,
                  received_data=sp.none
                  )
        self.add_flag('single-entry-point-annotation')

    @sp.entry_point
    def receive(self, params):
        sp.set_type(params, TCallback)
        sp.verify(sp.sender == self.data.fa2)
        sp.verify(params.sender == self.data.from_)
        sp.verify(params.receiver == sp.self_address)
        sp.verify(params.token_id == self.data.token_id)
        sp.verify(params.amount == self.data.amount)
        sp.verify(~self.data.received_data.is_some())
        self.data.received_data = sp.some(params.data)

    @sp.entry_point
    def default(self):
       pass


class Sender(sp.Contract):
    def __init__(self, admin):
        self.init(admin=admin)
        self.add_flag('single-entry-point-annotation')

    @sp.entry_point
    def send(self, fa2, batchs):
        sp.verify(sp.sender == self.data.admin)
        fa2_contract = sp.contract(
            TTransfer_and_call, fa2, entry_point="transfer_and_call")
        sp.transfer(batchs, sp.tez(0), fa2_contract.open_some())


if "templates" not in __name__:
    @sp.add_test(name="StoreValue")
    def test():
        fa2_admin = sp.test_account('admin')
        sender_admin = sp.test_account('sender_admin')
        receiver_admin = sp.test_account('receiver_admin')

        config = FA2.FA2_config()
        metadata = sp.map({"": sp.utils.bytes_of_string("")})

        fa2 = FA2_transfer_and_call(config, metadata, fa2_admin.address)

        sender = Sender(sender_admin.address)
        receiver = Receiver(fa2.address, sender.address, 0, 200)

        scenario = sp.test_scenario()
        scenario.h1("FA2 transfer and call")

        scenario.h2("Contracts")
        scenario.h3("FA2")
        scenario += fa2
        scenario.h3("Sender")
        scenario += sender
        scenario.h3("receiver")
        scenario += receiver

        scenario.h2("Admin mint some tokens")
        scenario += fa2.mint(
            address=sender.address,
            amount=500,
            token_id=0,
            metadata=sp.map({"": sp.utils.bytes_of_string("")})
        ).run(sender=fa2_admin)

        scenario.h2("Sender transfer_and_call")

        callback = sp.to_address(sp.contract(
            TCallback, receiver.address, entry_point="receive").open_some())
        txs = sp.record(to_=receiver.address, callback=callback,
                        data=sp.pack(sp.nat(42)), token_id=0, amount=200)
        batchs = sp.list([
                sp.record(
                    from_=sender.address,
                    txs=sp.list([txs])
                )
        ])
        scenario += sender.send(fa2=fa2.address,
                                batchs=batchs).run(sender=sender_admin)
